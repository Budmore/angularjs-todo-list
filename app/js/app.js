var app = angular.module('todoApp', [ 'ui.bootstrap', 'ui.sortable', 'md5']);

app.service('serviceTodo', function () {

    // TODO: Get this data from .json file, use $resource
    // http://www.jacopretorius.net/2013/04/using-ngresource-with-angularjs.html
    this.list = [
        {"task":"I'm awesome", "project":"Anakonda", "deadline":"10/21/2013", "done":true},
        {"task":"Create project", "project":"Zulu", "deadline":"10/21/2013", "done":false},
        {"task":"Read something about REST", "project":"TODO list", "deadline":"10/23/2013", "done":false}
    ];

    this.addToModel = function (item) {
        // TODO: Validate that item, if item.task exists
        this.list.push(item);
    };

    this.removeModel = function (item) {
        this.list.splice( this.list.indexOf(item), 1 );
    };

});

app.service('serviceAuth', function () {

    
    
})

var ModalInstanceCtrl = function ($scope, $modalInstance, serviceTodo, todo) {

    $scope.ok = function () {
        serviceTodo.removeModel(todo);
        $modalInstance.close();
    };

    $scope.cancel = function () {
        $modalInstance.close();
    };
};

app.controller('MainCtrl', function ($scope, $modal, serviceTodo) {

    // Remove, test only
    $scope.message = 'Hello World';
    
    // Model 
    $scope.todos = serviceTodo.list;

    $scope.addTodo = function (newTodo) {
        serviceTodo.addToModel(newTodo);
        delete $scope.newtodo;
    };

    $scope.removeTodo = function (todo) {
        var modalInstance = $modal.open({
            templateUrl: 'partials/modal-content.html',
            controller: ModalInstanceCtrl,
            resolve: {
                todo: function () {
                    return $scope.todo;
                }
            }
        });
    };

}); 


app.directive('loginForm', function ($http, md5) {
    return {
        restrict: 'E',
        scope: {},
        templateUrl: 'partials/login-form.html',
        controller: function ($scope) {

            $scope.auth = function () {

                // Origin ******* is not allowed by Access-Control-Allow-Origin.
                var url = 'http://62.89.99.14:8080/SaasManager-resources/resources/user/login';
                var email = $scope.model.email;
                var passwordHash = md5.createHash($scope.model.password);
                var postObject = {"email": email , "passwordHash": passwordHash};
                console.log(postObject);

                $http({
                    method : 'POST',
                    url : url,
                    data : postObject,
                    headers: {'Content-Type': 'application/json'}

                }).success(function(data, status, headers ){
                    console.log('yupi', data, headers);
                    alert('Login success');
                }).error(function(data, status, headers, config) {
                    console.log('error-100');
                });
            };
        }
    };
});


