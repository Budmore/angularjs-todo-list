"use strict";
describe("SimpleService", function(){

    beforeEach(module("todoApp"));

    var service;

    beforeEach(inject(function(serviceTodo){
        service = serviceTodo;
    
    }));

    describe("serviceTodo", function(){
        
        function countList() {
            return service.list.length;
        }   

        var ExampleObject = {
            task: "Test service", 
            deadline: "Wed Oct 16 2013 00:00:00 GMT+0200 (CEST)", 
            project: "unit tests"
        }; 


        // Add item
        it("should add object with task to the list", function(){
            var countBefore = countList();
            service.addToModel(ExampleObject);

            expect( countList() ).toBe( countBefore +1 );
            expect(service.addToModel).toThrow();
        });

        // Remove item
        it("should remove object from the list", function(){
            var countBefore = countList();
            service.removeModel(ExampleObject);

            expect( countList() ).toBe( countBefore -1 );
        });
    });
});