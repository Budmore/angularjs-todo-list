'use strict';

describe("controllers", function () {
    beforeEach(module("todoApp"));

    describe("MainCtrl", function () {
        var scope,
            controller;

        beforeEach(inject(function ($rootScope, $controller) {
            scope = $rootScope.$new();
            controller = $controller;
        }));

        it("should assign message to hello world", function () {
            controller("MainCtrl", {$scope: scope});
            expect(scope.message).toBe("Hello World");
        });

    });
});