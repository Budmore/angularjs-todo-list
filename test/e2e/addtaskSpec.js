var ptor;

describe('Todo app', function() {

  beforeEach(function() {
    ptor = protractor.getInstance();
    ptor.get('http://localhost:8000');
  });

  it('should init load 3 tasks', function() {
    ptor.findElements(protractor.By.repeater('todo in todos')).then(function(arr) {
      expect(arr.length).toEqual(3);
    });
  });

  it('should add new task to list', function() {
    var inputTask = ptor.findElement(protractor.By.input('newtodo.task'));
    var addButton = ptor.findElement(protractor.By.id('id_addtask'));

    inputTask.sendKeys('e2e tests rocks!');
    addButton.submit();

    //ptor.debugger();
    ptor.findElements(protractor.By.repeater('todo in todos')).then(function(arr) {
      expect(arr.length).toEqual(4);
    });
  });


});
