# ** TODO list ** - Angular 1.2  and Bootstrap 3.0#

Its simple one page application with some extra unit tests. Here it is live preview **[demo page]**.
This project is ready to run on your localhost with GruntJS. Check out my previous [Todo list] (jQuery + Bootstrap).  

##[Download] | [demo page]##


[demo page]: http://angular-todo-list.herokuapp.com/
[Download]: https://bitbucket.org/Technik/angularjs-todo-list/get/master.zip
[Todo list]: http://bootstraped-todo-list.herokuapp.com/