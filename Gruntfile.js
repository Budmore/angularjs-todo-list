module.exports = function(grunt) {

  // Project configuration.
  grunt.initConfig({
    connect: {
      server: {
        options: {
          port: 8000,
          base: 'app',
          keepalive: 'true'
        }
      }
    },

    jshint: {
      all: [
        // 'Gruntfile.js',
        // 'app/js/app.js',
        // 'test/unit/*.js',
        'test/e2e/*.js'
      ]
    },

    karma: {
      unit: {
        configFile: 'karma.conf.js',
        runnerPort: 9999,
        autoWatch: true,
        browsers: ['PhantomJS']
      }
    },
    shell: {
      options: {
        stdout: true
      },
      protractor: {
        command: 'node ./node_modules/protractor/bin/protractor ./protractor.conf.js'
      },
      install_selenium: {
        command: 'node ./node_modules/protractor/bin/install_selenium_standalone'
      }
    }

  });


  // Load the plugin that provides the "uglify" task.
  grunt.loadNpmTasks('grunt-contrib-connect');
  grunt.loadNpmTasks('grunt-contrib-jshint');
  grunt.loadNpmTasks('grunt-karma');
  grunt.loadNpmTasks('grunt-shell');

  // Default task(s).
  grunt.registerTask('default', ['connect']);
  grunt.registerTask('test:unit', ['jshint', 'karma']);

  // e2e with protractor
  grunt.registerTask('test:e2e', ['jshint', 'shell:protractor'])
  grunt.registerTask('install:selenium', ['shell:install_selenium'])
};
